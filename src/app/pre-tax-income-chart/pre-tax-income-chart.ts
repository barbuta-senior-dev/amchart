import { Component, Inject, NgZone, PLATFORM_ID, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'pre-tax-Chart',
  templateUrl: './pre-tax-income-chart.html',
  styleUrls: ['./pre-tax-income-chart.scss']
})

export class PreTaxIncomeChartComponent {
  currentTab: any;
  @Input() annumChartData: any;
  @Input() quarterChartData: any;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) { }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.currentTab = "annum";
    this.browserOnly(() => {
      am4core.useTheme(am4themes_animated);

      /* Create Annum Chart */

      let annum_chart = am4core.create("annumContainer", am4charts.XYChart);
      annum_chart.data = this.annumChartData;

      /* Create Axis */

      let categoryAxis = annum_chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis.dataFields.category = "year";
      categoryAxis.renderer.grid.template.disabled = true;
      categoryAxis.fontSize = 12;
      categoryAxis.renderer.inversed = true;
      categoryAxis.renderer.labels.template.fill = am4core.color("#7C828A");

      let valueAxis = annum_chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.min = 0;
      valueAxis.renderer.minGridDistance = 30;
      valueAxis.numberFormatter = new am4core.NumberFormatter();
      valueAxis.numberFormatter.numberFormat = "#'%'";
      valueAxis.fontSize = 12;
      valueAxis.renderer.labels.template.fill = am4core.color("#7C828A");
      valueAxis.calculateTotals = true;
      /* Create Color List */
      annum_chart.colors.list = [
        am4core.color("#4F84CE"),
        am4core.color("#FA9622"),
      ];

      /* Create Series */
      createSeries(annum_chart, "cash", "Cash", false, true);
      createSeries(annum_chart, "credits", "Franking Credits", true, true);

      /* Create Annum Legend */
      let annum_legendContainer = am4core.create("annum-legendContainer", am4core.Container);
      annum_legendContainer.width = am4core.percent(100);
      annum_legendContainer.height = am4core.percent(100);

      annum_chart.legend = new am4charts.Legend();
      annum_chart.legend.useDefaultMarker = true;
      annum_chart.legend.labels.template.fontSize = 14;
      annum_chart.legend.labels.template.fill = am4core.color("#7C828A")
      annum_chart.legend.position = "absolute";
      annum_chart.legend.contentAlign = "right";
      annum_chart.legend.dy = -10;
      annum_chart.legend.parent = annum_legendContainer;
      
      var annum_marker: any = annum_chart.legend.markers.template.children.getIndex(0);
      annum_marker.cornerRadius(4, 4, 4, 4);
      annum_marker.width = 8;
      annum_marker.height = 8;
      annum_marker.dy = 6;
      annum_marker.strokeWidth = 0;

      /* Create Quarter Chart */
      let quarter_chart = am4core.create("quarterContainer", am4charts.XYChart);
      quarter_chart.data = this.quarterChartData;

      /* Create Axis */
      let categoryAxis_q = quarter_chart.xAxes.push(new am4charts.CategoryAxis());
      categoryAxis_q.dataFields.category = "year";
      categoryAxis_q.renderer.grid.template.disabled = true;
      categoryAxis_q.fontSize = 12;
      categoryAxis_q.renderer.minGridDistance = 20;
      categoryAxis_q.renderer.inversed = true;
      categoryAxis_q.renderer.labels.template.fill = am4core.color("#7C828A");
 

      let valueAxis_q = quarter_chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis_q.min = 0;
      valueAxis_q.renderer.minGridDistance = 30;
      valueAxis_q.numberFormatter = new am4core.NumberFormatter();
      valueAxis_q.numberFormatter.numberFormat = "#'%'";
      valueAxis_q.fontSize = 12;
      valueAxis_q.renderer.labels.template.fill = am4core.color("#7C828A");
      valueAxis_q.calculateTotals = true;
      /* Create Color List */
      quarter_chart.colors.list = [
        am4core.color("#4F84CE"),
        am4core.color("#FA9622"),
      ];

      /* Create Quarter Legend */
      let quarter_legendContainer = am4core.create("quarter-legendContainer", am4core.Container);
      quarter_legendContainer.width = am4core.percent(100);
      quarter_legendContainer.height = am4core.percent(100);

      quarter_chart.legend = new am4charts.Legend();
      quarter_chart.legend.useDefaultMarker = true;
      quarter_chart.legend.labels.template.fontSize = 14;
      quarter_chart.legend.labels.template.fill = am4core.color("#7C828A")
      quarter_chart.legend.position = "absolute";
      quarter_chart.legend.contentAlign = "right";
      quarter_chart.legend.dy = -10;
      quarter_chart.legend.parent = quarter_legendContainer;

      var quarter_marker: any = quarter_chart.legend.markers.template.children.getIndex(0);
      quarter_marker.cornerRadius(4, 4, 4, 4);
      quarter_marker.width = 8;
      quarter_marker.height = 8;
      quarter_marker.dy = 6;
      quarter_marker.strokeWidth = 0;

      /* Create Series */
      createSeries(quarter_chart, "cash", "Cash", false, false);
      createSeries(quarter_chart, "credits", "Franking Credits", true, false);

      /* Create Seriese function */
      function createSeries(chart, field, name, stacked, showLabel) {
        let series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "year";
        series.name = name;
        series.stacked = stacked;

        if (showLabel) {
          series.columns.template.width = am4core.percent(60);
        } else {
          series.columns.template.width = am4core.percent(70);
        }

        series.stroke = am4core.color("#fff");
        series.strokeWidth = 1;

        if (showLabel) {
          let valueLabel = series.columns.template.createChild(am4core.Label);
          valueLabel.text = "{valueY}%";
          valueLabel.fontSize = 14;
          valueLabel.fill = am4core.color("#fff")
          valueLabel.align = "center";
          valueLabel.valign = "middle";
          valueLabel.strokeWidth = 0;

          series.tooltip.label.textAlign = "start";
          series.tooltip.maxWidth = 10000;
          series.tooltip.label.fontSize = 12;
          series.tooltip.label.fill = am4core.color("#000");
          series.tooltip.pointerOrientation = "vertical";
          series.tooltip.getFillFromObject = false;
          series.tooltip.background.fill = am4core.color("#fff");
          series.columns.template.tooltipY = am4core.percent(0);
          series.columns.template.tooltipHTML = `<span style='font-size: 14px; font-weight: 700;'>{categoryX}</span>
                                                  <table>
                                                  <tr>
                                                    <td style="border:0 !important; font-size: 12px !important; color: #313131 !important;">Total:</td>
                                                    <td style="border:0 !important; font-size: 12px !important; font-weight:700 !important;  color:#313131 !important; padding-left: 10px !important">{valueY.total}%</td>
                                                  </tr>
                                                  <tr>
                                                    <td style="border:0 !important; font-size: 12px !important; color:#313131 !important;">Cash:</td>
                                                    <td style="border:0 !important; font-size: 12px !important; font-weight:700 !important; color:#313131 !important; padding-left: 10px !important">{cash}%</td>
                                                  </tr>
                                                  <tr>
                                                    <td style="border:0 !important; font-size: 12px !important; color:#313131 !important;">Franking Credits:</td>
                                                    <td style="border:0 !important; font-size: 12px !important; font-weight:700 !important; color:#313131 !important; padding-left: 10px !important">{credits}%</td>
                                                  </tr>
                                                  </table>                                      
                                                  `;
          let shadow:any = series.tooltip.background.filters.getIndex(0);
          shadow.dx = 0;
          shadow.dy = 1;
          shadow.blur = 8;
          shadow.color = am4core.color("#539B55DD");

          // Create series for total
          
        }
        if(field == 'credits'){

          var totalSeries = chart.series.push(new am4charts.ColumnSeries());
          totalSeries.dataFields.valueY = "none";
          totalSeries.dataFields.categoryX = "year";
          totalSeries.stacked = true;
          totalSeries.hiddenInLegend = true;
          totalSeries.columns.fillOpacity = 0;
          totalSeries.columns.template.strokeOpacity = 0;
          
          var totalBullet = totalSeries.bullets.push(new am4charts.LabelBullet());
          totalBullet.dy = -10;
          totalBullet.label.text = "{valueY.total}%";
          totalBullet.label.hideOversized = false;
          totalBullet.label.fontSize = 14;
          totalBullet.label.background.fill = totalSeries.stroke;
          totalBullet.label.background.fillOpacity = 0;
          // totalBullet.label.padding(5, 10, 5, 10);
        }
      }
    });
  }

  ngOnDestroy() {
    
  }

  annumClick() {
    this.currentTab = "annum";

    var annumTab = document.getElementById('annumTab');
    var quarterTab = document.getElementById('quarterTab');
    var annumLegend = document.getElementById('annum-legendContainer');
    var quarterLegend = document.getElementById('quarter-legendContainer');
    var annumContainer = document.getElementById('annumContainer');
    var quarterContainer = document.getElementById('quarterContainer');

    annumTab.classList.add('active')
    quarterTab.classList.remove('active')

    annumLegend.classList.add('show')
    quarterLegend.classList.remove('show');

    annumContainer.classList.add("show");
    quarterContainer.classList.remove("show");
  }

  quarterClick() {
    this.currentTab = "quarter";

    var annumTab = document.getElementById('annumTab');
    var quarterTab = document.getElementById('quarterTab');
    var annumLegend = document.getElementById('annum-legendContainer');
    var quarterLegend = document.getElementById('quarter-legendContainer');
    var annumContainer = document.getElementById('annumContainer');
    var quarterContainer = document.getElementById('quarterContainer');

    annumTab.classList.remove('active');
    quarterTab.classList.add('active');

    annumLegend.classList.remove('show');
    quarterLegend.classList.add('show');

    annumContainer.classList.remove("show");
    quarterContainer.classList.add("show");
  }
}