import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreTaxIncomeChartComponent } from './pre-tax-income-chart';

describe('ColumnChartComponent', () => {
  let component: PreTaxIncomeChartComponent;
  let fixture: ComponentFixture<PreTaxIncomeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreTaxIncomeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreTaxIncomeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
