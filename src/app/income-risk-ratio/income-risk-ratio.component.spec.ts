import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomeRiskRatioComponent } from './income-risk-ratio.component';

describe('IncomeRiskRatioComponent', () => {
  let component: IncomeRiskRatioComponent;
  let fixture: ComponentFixture<IncomeRiskRatioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomeRiskRatioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomeRiskRatioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
