import { Component, Inject, NgZone, PLATFORM_ID, Input} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'income-risk-ratio-chart',
  templateUrl: './income-risk-ratio.component.html',
  styleUrls: ['./income-risk-ratio.component.scss']
})

export class IncomeRiskRatioComponent {
  @Input() cpId: any;
  @Input() title: any;
  @Input() description: any;
  @Input() chartData: any;
  @Input() xField: any;
  @Input() yField: any;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) { }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    am4core.useTheme(am4themes_animated);
    // Themes end
    const xField = this.xField;
    const yField = this.yField;

    // Create chart instance
    let chart = am4core.create(this.cpId, am4charts.XYChart);
    chart.width = am4core.percent(100);

    // Add data
    chart.data = this.chartData;

    // Create axes
    let xAxis = chart.xAxes.push(new am4charts.ValueAxis());
    xAxis.renderer.minGridDistance = 60;
    xAxis.renderer.grid.template.location = 0;
    xAxis.renderer.grid.template.strokeWidth = 0;
    xAxis.renderer.labels.template.fill = am4core.color("#7C828A");
    xAxis.numberFormatter = new am4core.NumberFormatter();
    xAxis.numberFormatter.numberFormat = "#'%'";
    xAxis.fontSize = 12;
    xAxis.title.text = xField + " p.a";
    xAxis.title.fontSize = 14;    
    xAxis.renderer.grid.template.disabled = true;
    xAxis.renderer.labels.template.disabled = true;
    xAxis.title.fill = am4core.color("#7C828A");
    xAxis.title.paddingTop =15;
    xAxis.max = chart.data[chart.data.length - 1][xField] + 1;

    let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
    yAxis.numberFormatter.numberFormat = "#'%'";
    yAxis.fontSize = 12;
    yAxis.renderer.minGridDistance = 60;
    yAxis.renderer.labels.template.fill = am4core.color("#7C828A");
    yAxis.renderer.grid.template.disabled = true;
    yAxis.renderer.labels.template.disabled = true;
    yAxis.title.text = yField + " p.a";
    yAxis.title.fontSize = 14;
    yAxis.title.fill = am4core.color("#7C828A");
    yAxis.title.paddingBottom = 30;
    // Create series
    let series: any = chart.series.push(new am4charts.LineSeries());
    series.dataFields.valueY = yField;
    series.dataFields.valueX = xField;
    series.dataFields.name = "name";
    series.strokeWidth = 0;

    let bullet = series.bullets.push(new am4charts.CircleBullet());
    bullet.strokeWidth = 0;
    bullet.circle.fill = am4core.color("#4F84CE");
    bullet.stroke = am4core.color("#4F84CE");

    //Create Label
    let bulletLabel = series.bullets.push(new am4charts.LabelBullet());
    bulletLabel.label.text = "{name}";
    bulletLabel.label.fontSize = 12;
    bulletLabel.label.horizontalCenter = "left";
    bulletLabel.label.dx = 12;
    bulletLabel.label.padding(4, 8, 4, 8);
    bulletLabel.adapter.add("dy", function(dy, target) {
      if (target.dataItem && target.dataItem.index == chart.data.length - 1) {
        target.label.background.fill = am4core.color("#4F84CE");
        target.label.fill = am4core.color("#fff");
      }
      return dy;
    })

    function createTrendLine(data) {
      let trend = chart.series.push(new am4charts.LineSeries());
      trend.dataFields.valueY = yField;
      trend.dataFields.valueX = xField;
      trend.strokeWidth = 2;
      trend.strokeDasharray = "3,3";
      trend.stroke = trend.fill = am4core.color("#4F84CE");
      trend.data = data;

      return trend;
    };
    
    function createYGrid(value) {
      var range = yAxis.axisRanges.create();
      range.value = value;
      range.label.text = "{value}";
    }

    function createXGrid(value) {
      var range = xAxis.axisRanges.create();
      range.value = value;
      range.label.text = "{value}";
    }
    if(yField == "MaxDrawDown"){
      for (var i = 0 ; i < 7 ; i ++){
        createYGrid(-i * 10)
      }
      for(var i = 1 ; i < 8; i ++){
        createXGrid(i)
      }
    }else {

      for (var i = 0 ; i < 7 ; i ++){
        createYGrid(i * 5)
      }
      for(var i = 1 ; i < 8; i ++){
        createXGrid(i)
        createXGrid(i + 0.5)
      }
    }

    
    
    createXGrid(8)
    let trendLineData = '[{"' + xField + '":' + this.chartData[0][xField] + ',"' + yField + '":' + (this.chartData[0][yField] + this.chartData[1][yField]) / 2 + '},{"'+
                                xField + '":' + this.chartData[this.chartData.length - 1][xField] + ',"' + yField + '":' + this.chartData[this.chartData.length - 3][yField] + '}]';
    createTrendLine(JSON.parse(trendLineData));
  }

  ngOnDestroy() {

  }
}
