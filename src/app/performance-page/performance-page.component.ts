import { Component } from '@angular/core';

@Component({
  selector: 'app-performance-page',
  templateUrl: './performance-page.component.html',
  styleUrls: ['./performance-page.component.scss']
})
export class PerformancePageComponent{
  annumChartData = [{
    "year": "FY 2018",
    "cash": 4.06,
    "credits": 2.07,
    "none":0
  }, {
    "year": "FY 2019",
    "cash": 5.72,
    "credits": 2.53,
    "none":0
  }, {
    "year": "FY 2020",
    "cash": 5.19,
    "credits": 1.62,
    "none":0
  }];

  quarterChartData = [{
    "year": "Sep-18 Qtr",
    "cash": 1.00,
    "credits": 1.2,
    "none":0
  }, {
    "year": "Dec-18 Qtr",
    "cash": 1.00,
    "credits": 0.35,
    "none":0
  }, {
    "year": "Mar-19 Qtr",
    "cash": 1.00,
    "credits": 0.86,
    "none":0
  }, {
    "year": "Jun-19 Qtr",
    "cash": 2.61,
    "credits": 0.17,
    "none":0
  }, {
    "year": "Sep-19 Qtr",
    "cash": 1.00,
    "credits": 0.93,
    "none":0
  }, {
    "year": "Dec-19 Qtr",
    "cash": 1.00,
    "credits": 0.31,
    "none":0
  }, {
    "year": "Mar-20 Qtr",
    "cash": 1.00,
    "credits": 0.75,
    "none":0
  }, {
    "year": "Jun-20 Qtr",
    "cash": 2.09,
    "credits": 0.0,
    "none":0
  }];
  volatilityData=[
  {
    "name": "Cash",
    "Yield": 1.3,
    "Volatility": 1,
  },{
    "name": "Aust IG Bonds",
    "Yield": 3.1,
    "Volatility": 6,
  },{
    "name": "Global Infrastructure",
    "Yield": 3.6,
    "Volatility": 18,
  },{
    "name": "Hybrids",
    "Yield": 4.3,
    "Volatility": 8,
  },{
    "name": "ASX-200(Franked)",
    "Yield": 6,
    "Volatility": 19,
  },{
    "name": "A-REIT",
    "Yield": 6.2,
    "Volatility": 28,
  },{
    "name": "Pentalpha",
    "Yield": 7,
    "Volatility": 4,
  },];
  drawdownData=[
    {
      "name": "Cash",
      "Yield": 1.3,
      "MaxDrawDown": -2,
    },{
      "name": "Aust IG Bonds",
      "Yield": 3.1,
      "MaxDrawDown": -4.43,
    },{
      "name": "Global Infrastructure",
      "Yield": 3.6,
      "MaxDrawDown": -16.67,
    },{
      "name": "Hybrids",
      "Yield": 4.3,
      "MaxDrawDown": -26.67,
    },{
      "name": "ASX-200(Franked)",
      "Yield": 6,
      "MaxDrawDown": -36.67,
    },{
      "name": "A-REIT",
      "Yield": 6.2,
      "MaxDrawDown": -48.33,
    },{
      "name": "Pentalpha",
      "Yield": 7,
      "MaxDrawDown": -4,
    },];

  monthlyPerformanceData=[{
    "year": "Jun 17",
    "unit-price": 1.001,
    "monthly-performance": 0,
  }, {
    "year": "Jul 17",
    "unit-price": 1.01,
    "monthly-performance": 1,
  }, {
    "year": "Aug 17",
    "unit-price": 1.01,
    "monthly-performance": 0.1,
  }, {
    "year": "Sep 17",
    "unit-price": 1.01,
    "monthly-performance": 0.1,
  }, {
    "year": "Oct 17",
    "unit-price": 1.01,
    "monthly-performance": 1.5,
  }, {
    "year": "Nov 17",
    "unit-price": 1.025,
    "monthly-performance": 0.5,
  }, {
    "year": "Dec 17",
    "unit-price": 1.035,
    "monthly-performance": 0.2,
  }, {
    "year": "Jan 18",
    "unit-price": 1.04,
    "monthly-performance": -0.5,
  }, {
    "year": "Feb 18",
    "unit-price": 1.03,
    "monthly-performance": 0.8,
  }, {
    "year": "Mar 18",
    "unit-price": 1.032,
    "monthly-performance": -1.4,
  }, {
    "year": "Apr 18",
    "unit-price": 1.018,
    "monthly-performance": 0.2,
  }, {
    "year": "May 18",
    "unit-price": 1.02,
    "monthly-performance": 0.15,
  }, {
    "year": "Jun 18",
    "unit-price": 1.019,
    "monthly-performance": 0.6,
  }, {
    "year": "Jul 18",
    "unit-price": 1.025,
    "monthly-performance": 0.5,
  }, {
    "year": "Aug 18",
    "unit-price": 1.035,
    "monthly-performance": 0.8,
  }, {
    "year": "Sep 18",
    "unit-price": 1.047,
    "monthly-performance": -1.2,
  }, {
    "year": "Oct 18",
    "unit-price": 1.028,
    "monthly-performance": -2,
  }, {
    "year": "Nov 18",
    "unit-price": 1.008,
    "monthly-performance": -0.3,
  }, {
    "year": "Dec 18",
    "unit-price": 1.001,
    "monthly-performance": -0.4,
  }, {
    "year": "Jan 19",
    "unit-price": 1.008,
    "monthly-performance": 0.4,
  }, {
    "year": "Feb 19",
    "unit-price": 1.02,
    "monthly-performance": 1.2,
  }, {
    "year": "Mar 19",
    "unit-price": 1.028,
    "monthly-performance": 1.1,
  }, {
    "year": "Apr 19",
    "unit-price": 1.035,
    "monthly-performance": 0.9,
  }, {
    "year": "May 19",
    "unit-price": 1.05,
    "monthly-performance": 1,
  }, {
    "year": "Jun 19",
    "unit-price": 1.06,
    "monthly-performance": 1.5,
  }, {
    "year": "Jul 19",
    "unit-price": 1.07,
    "monthly-performance": 1.2,
  }, {
    "year": "Aug 19",
    "unit-price": 1.09,
    "monthly-performance": 0.8,
  }, {
    "year": "Sep 19",
    "unit-price": 1.075,
    "monthly-performance": 0.8,
  }, {
    "year": "Oct 19",
    "unit-price": 1.08,
    "monthly-performance": 1.4,
  }, {
    "year": "Nov 19",
    "unit-price": 1.07,
    "monthly-performance": -1.1,
  }, {
    "year": "Dec 19",
    "unit-price": 1.06,
    "monthly-performance": 1,
  }];

  DistributedClicked(){
    this.ScrollTo("distributed-chart");
    
    var distributeBtn = document.getElementById('distributeBtn');
    distributeBtn.classList.add('active')
  }

  IncomeRiskClicked(){
    this.ScrollTo("income-tax");
    
    var incomeBtn = document.getElementById('incomeBtn');
    incomeBtn.classList.add('active')
  }

  TotalReturnClicked(){
    this.ScrollTo("total-return");
    
    var totalReturnBtn = document.getElementById('totalReturnBtn');
    totalReturnBtn.classList.add('active')
  }

  MonthlyReportClicked() {
    this.ScrollTo("monthly-report");
    
    var monthlyBtn = document.getElementById('monthlyBtn');
    monthlyBtn.classList.add('active')
  }

  ScrollTo(id){
    var distributeBtn = document.getElementById('distributeBtn');
    var incomeBtn = document.getElementById('incomeBtn');
    var totalReturnBtn = document.getElementById('totalReturnBtn');
    var monthlyBtn = document.getElementById('monthlyBtn');

    distributeBtn.classList.remove('active')
    incomeBtn.classList.remove('active')
    totalReturnBtn.classList.remove('active')
    monthlyBtn.classList.remove('active')

    const yOffset = -120; 
    const element = document.getElementById(id);
    const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
    window.scrollTo({top: y, behavior: 'smooth'});
  }
}

