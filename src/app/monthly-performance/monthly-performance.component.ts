import { Component, Inject, NgZone, PLATFORM_ID, Input } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';

@Component({
  selector: 'monthly-performance-chart',
  templateUrl: './monthly-performance.component.html',
  styleUrls: ['./monthly-performance.component.scss']
})

export class MonthlyPerformanceComponent {
  @Input() title: any;
  @Input() description: any;
  @Input() chartData: any;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone) { }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    am4core.useTheme(am4themes_animated);
    // Themes end
  
    // Create chart instance
    let chart = am4core.create("monthly-chart-container", am4charts.XYChart);
    chart.width = am4core.percent(100);
    // Data for both series
    chart.data = this.chartData;
  
    /* Create axes */
  
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.fontSize = 12;
    categoryAxis.cursorTooltipEnabled = false;
    categoryAxis.renderer.minGridDistance = 20;
    categoryAxis.renderer.fullWidthTooltip = true;
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.labels.template.location = 0.5;
    categoryAxis.renderer.labels.template.fill = am4core.color("#7C828A");
    categoryAxis.renderer.labels.template.adapter.add("opacity", function(opacity, target) {
      if (target.dataItem && target.dataItem.index % 2 == 1) {
        return 0;
      }
      return opacity;
    });

    /* Create Colors */
    chart.colors.list = [
      am4core.color("#FA9622"),
      am4core.color("#4F84CE"),
    ];

    /* Create value axis2 */
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.grid.template.strokeWidth = 0;
    valueAxis.renderer.minGridDistance = 20;
    valueAxis.numberFormatter = new am4core.NumberFormatter();
    valueAxis.numberFormatter.numberFormat = "$#";
    valueAxis.min = 0.8;
    valueAxis.max = 1.16;
    valueAxis.fontSize = 12;
    valueAxis.cursorTooltipEnabled = false;
    valueAxis.renderer.grid.template.disabled = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.renderer.labels.template.fill = am4core.color("#FA9622");

    /* Create series */
    let lineSeries = chart.series.push(new am4charts.LineSeries());
    lineSeries.zIndex = 1;
    lineSeries.name = "Unit Price (Cum Distribution)";
    lineSeries.dataFields.valueY = "unit-price";
    lineSeries.dataFields.categoryX = "year";
    lineSeries.strokeWidth = 3;
    lineSeries.tooltipText = "{valueY}";
    
    let unitBullet = lineSeries.bullets.push(new am4charts.CircleBullet());
    unitBullet.circle.fill = am4core.color("#fff");
    unitBullet.circle.strokeWidth = 0;
    unitBullet.circle.radius = 1;
    unitBullet.circle.fill = am4core.color("#FA9622");

    // lineSeries.tooltip.maxWidth = 10000;
    lineSeries.tooltip.label.textAlign = "start";
    lineSeries.tooltip.label.fontSize = 12;
    lineSeries.tooltip.label.fill = am4core.color("#000");
    lineSeries.tooltip.pointerOrientation = "vertical";
    lineSeries.tooltip.getFillFromObject = false;
    lineSeries.tooltip.background.fill = am4core.color("#fff");
    lineSeries.tooltip.dy = -10;
    lineSeries.tooltipHTML = `<span style='font-size: 14px; font-weight: 700;'>{categoryX}</span>
                                          <table>
                                            <tr style="margin:0; padding: 0">
                                              <td style="border:0 !important; font-size: 12px !important; color:#313131 !important;">Unit Price (Cum Distribution):</td>
                                              <td style="border:0 !important; font-size: 12px !important; font-weight:700 !important; color:#313131 !important; padding-left: 10px !important">{unit-price}%</td>
                                            </tr>
                                            <tr style="margin:0; padding: 0">
                                              <td style="border:0 !important; font-size: 12px !important; color:#313131 !important;">Monthly Performance (Net of Fees):</td>
                                              <td style="border:0 !important; font-size: 12px !important; font-weight:700 !important; color:#313131 !important; padding-left: 10px !important">{monthly-performance}%</td>
                                            </tr>
                                          </table>                                      
                                            `;
    let shadow:any = lineSeries.tooltip.background.filters.getIndex(0);
    shadow.dx = 0;
    shadow.dy = 1;
    shadow.blur = 8;
    shadow.color = am4core.color("#539B55DD");

    let unitState = unitBullet.states.create("hover");
    unitState.properties.scale = 5;
    // unitState.properties.circleR = 2;

    /* Create value axis2 */
    let valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis2.fontSize = 12;
    valueAxis2.cursorTooltipEnabled = false;
    valueAxis2.numberFormatter = new am4core.NumberFormatter();
    valueAxis2.numberFormatter.numberFormat = "#'%'";
    valueAxis2.renderer.opposite = true;
    valueAxis2.renderer.grid.template.strokeWidth = 0;
    valueAxis2.renderer.grid.template.disabled = true;
    valueAxis2.renderer.labels.template.disabled = true;
    valueAxis2.renderer.labels.template.fill = am4core.color("#4F84CE");
    
    /* Create series */
    let columnSeries = chart.series.push(new am4charts.ColumnSeries());
    columnSeries.zIndex = 0;
    columnSeries.name = "Monthly Performance (Net of Fees)";
    columnSeries.dataFields.valueY = "monthly-performance";
    columnSeries.dataFields.categoryX = "year";
    columnSeries.strokeWidth = 0;
    columnSeries.yAxis = valueAxis2;
    columnSeries.columns.template.width = am4core.percent(50);
    
    /* Create Legend */
    chart.legend = new am4charts.Legend();
    chart.legend.useDefaultMarker = true;
    chart.legend.labels.template.fontSize = 14;
    chart.legend.labels.template.fill = am4core.color("#7C828A");
    chart.legend.position = "top";
    chart.legend.percentWidth = 100;
    chart.legend.itemContainers.template.width = am4core.percent(45);
    chart.legend.itemContainers.template.contentAlign = "left";
    chart.legend.padding(0, 0, 30, 0);
    chart.legend.margin(0, 0, 0, 0);

    chart.legend.itemContainers.template.adapter.add("contentAlign", function(contentAlign, target) {
      if (target.dataItem && target.dataItem.index == 1) {
        return "right";
      }
      return contentAlign;
    });

    var marker: any = chart.legend.markers.template.children.getIndex(0);
    marker.cornerRadius(4, 4, 4, 4);
    marker.width = 8;
    marker.height = 8;
    marker.dy = 6;
    marker.strokeWidth = 0;

    chart.cursor = new am4charts.XYCursor();
    chart.cursor.fullWidthLineX = true;
    chart.cursor.xAxis = categoryAxis;
    chart.cursor.lineX.strokeOpacity = 0;
    chart.cursor.lineY.strokeOpacity = 0;
    chart.cursor.lineX.fill = am4core.color("#000");
    chart.cursor.lineX.fillOpacity = 0.1;

    function createRightGrid(value) {
      var range = valueAxis2.axisRanges.create();
      range.value = value;
      range.label.text = "{value}";
    }

    function createLeftGrid(value) {
      var range = valueAxis.axisRanges.create();
      range.value = value;
      range.label.text = "{value}";
    }

    for(var i = 0 ; i < 10; i ++){
      createRightGrid( 2 - i * 0.5);
    }
    for(var i = 0 ; i <= 18; i ++){  
      var value = 0.8 + i * 0.02;
      createLeftGrid( value.toFixed(2));
    }

  }

  ngOnDestroy() {

  }
}