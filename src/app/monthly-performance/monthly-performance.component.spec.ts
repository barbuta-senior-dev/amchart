import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyPerformanceComponent } from './monthly-performance.component';

describe('MonthlyPerformanceComponent', () => {
  let component: MonthlyPerformanceComponent;
  let fixture: ComponentFixture<MonthlyPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MonthlyPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
