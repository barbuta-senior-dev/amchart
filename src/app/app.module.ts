import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';

import { PreTaxIncomeChartComponent } from './pre-tax-income-chart/pre-tax-income-chart';
import { IncomeRiskRatioComponent } from './income-risk-ratio/income-risk-ratio.component';
import { MonthlyPerformanceComponent } from './monthly-performance/monthly-performance.component';
import { PerformancePageComponent } from './performance-page/performance-page.component';
import { BlankPageComponent } from './blank-page/blank-page.component';

const routes: Routes = [
  { path: '', component: BlankPageComponent },
  { path: 'performance', component: PerformancePageComponent },
];

@NgModule({
  
  declarations: [
    AppComponent,
    PreTaxIncomeChartComponent,
    IncomeRiskRatioComponent,
    MonthlyPerformanceComponent,
    PerformancePageComponent,
    BlankPageComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
